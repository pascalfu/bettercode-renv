# BetterCode renv

## Initialization

- `.libPaths()`
- `renv::init()`
- `.libPaths()`

## Installing

- `renv::install("package@2.3.4")` install specific package version
- `renv::install("githubuser/package", ref = "<commit hash>")` install package from GitHub, optionally provide commit hash
- `renv::install("/p/tmp/username/yourpackagefolder")` install package from sources
- `renv::remove("package")` uninstall package
- `renv::hydrate("package")` copy system installation of package into renv, fallback to renv::install

## Lockfiles

- `renv::snapshot()` write renv.lock according to snapshot type
- `renv::status()` show differences between library and renv.lock
- `piamenv::archiveRenv()` (`make archive-renv`) write renv.lock to renv/archive

## Updating

- `piamenv::updateRenv()` (`make update-renv`) update all piam packages
- `renv::update(exclude = "renv")` (make update-renv-all) update all packages except renv (please do not update renv itself)
- `renv::update("package")` update package

## Restoring

- `renv::restore("path/to/renv.lock")` install all packages as defined in given lockfile
- `piamenv::restoreRenv()` restore lockfile from `renv/archive/*_renv.lock` or `output/*/renv.lock`


## Script workflow

- [renv::use("digest", "rlang@0.3.4")](https://rstudio.github.io/renv/articles/use.html) use a specific package version in a script
```
renv::use('poorman@0.2.5')
print(.libPaths())
print(packageVersion('poorman'))
```
- [renv::embed()](https://rstudio.github.io/renv/reference/embed.html) put a `renv::use` call into a script ensuring all current package version are being used

## Links

- [REMIND renv tutorial](https://github.com/remindmodel/remind/blob/develop/tutorials/11_ManagingRenv.md)
- [MAgPIE renv tutorial](https://magpiemodel.github.io/tutorials/t04-renv)
- [lockfile archive on GitHub](https://github.com/pik-piam/lockfile-archive)
- [renv package documentation](https://rstudio.github.io/renv/)
